# Turtlebot remote
Catkin workspace of turtlebot remote.

For installation see:
http://emanual.robotis.com/docs/en/platform/turtlebot3/overview/#overview


# Example
[Remote PC]
```
roscore
```


For SSH to client from remote pc:
```
turtlepi@xxx.xxx.xx.xx
password: a
```

[TurtlebotClient]
```
roslaunch turtlebot3_bringup turtlebot3_robot.launch
```

[Remote PC]
```
export TURTLEBOT3_MODEL=${TB3_MODEL}
roslaunch turtlebot3_slam turtlebot3_slam.launch slam_methods:=gmapping
```